/*
    This file is part of intelligend-eco.

    Copyright(c) 2015 Michael Kankkonen

    Inteligend-eco is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intelligend-eco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with intelligend-eco.  If not, see <http://www.gnu.org/licenses/>.
*/

#define PCRE2_CODE_UNIT_WIDTH 8
#include <pcre2.h>


#include <zmq.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include "intelligend-common.h"

EcoClusterConf *clusterConf;

int defBufferSize = 1024;
int INFO = 0;
int ERROR = 10;

void eco_log(int level, char *logRow){
   printf("logRow: %s\n", logRow);
}


int eco_init(char *confFile){
  int iRet = -1;
  clusterConf = (EcoClusterConf*) malloc(sizeof(EcoClusterConf));
  char logRow[defBufferSize];
  sprintf(logRow, "Using configuration file: %s", confFile);
  eco_log(INFO, logRow);
  iRet = parseClusterConf(confFile, clusterConf);
  if( iRet != ECO_CONF_OK ){
      sprintf(logRow, "Error while handling configuration file: %s", confFile);
      eco_log(ERROR, logRow);
      exit(iRet);
  }
}

int eco_close(){
  free(clusterConf);
}

// we start with one listener first. If this doesn't work properly we split the handling to 
// several thread (status own, file handling own etc.)
int eco_start ()
{
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, "tcp://*:5555");

    assert (rc == 0);
    int iBuffer = clusterConf->bufferSize;
    char buffer [iBuffer];

/*
    while (1) {
        zmq_recv (responder, buffer, iBuffer, 0);
        printf ("Received %s\n", buffer);
        zmq_recv (responder, buffer, iBuffer, 0);
        printf ("Received %s\n", buffer);
        sleep (1);
        zmq_send (responder, "World", 5, 0);
        printf ("Resoponse sent.\n");
        zmq_close(responder);
        zmq_ctx_destroy (context);
        return 0;
    }
*/

    helloWorld("Common test text...");

    zmq_close(responder);
    zmq_ctx_destroy (context);
    return 0;
}


int main(int argc, char **args){
   init_perl();
   int iRet = eco_replace("abc", "auto abc maatti");

/*
   eco_init("/etc/intelligend-eco/cluster.conf");
   eco_start();
   eco_close();
   printf("last error: %s\n", ECO_LAST_ERROR);
*/


   free_perl();
}

