/*
    This file is part of intelligend-eco.

    Copyright(c) 2015 Michael Kankkonen

    Inteligend-eco is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intelligend-eco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with intelligend-eco.  If not, see <http://www.gnu.org/licenses/>.
*/

#define PCRE2_CODE_UNIT_WIDTH 8

#include <EXTERN.h>
#include <perl.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <pcre2.h>
#include <regex.h>



#include "intelligend-common.h"

int ECO_CONF_OK = 0;
int ECO_CONF_ERR_FILE_NO_FOUND = 1;

char *ECO_LAST_ERROR;

static PerlInterpreter *my_perl;


int init_perl(){
  //PERL_SYS_INIT3(&argc,&argv,&env);
  char *embedding[] = { "", "script/regex.pl" }; 
  my_perl = perl_alloc();
  perl_construct( my_perl ); 
  perl_parse(my_perl, NULL, 2, embedding, NULL);

  return 0;
}

int free_perl(){
  perl_destruct(my_perl); 
  perl_free(my_perl);
}


// This is reraley used feature so using
// perl in here doesn't have any impact on performacnei.
// The same apply to all regex functions implemented here.
// We just want ot use the fantastic perl feature of regex here
// and later the Lucy (lucene c interface) via perl.
int eco_match(char *regexParam, char *strParam){

/* embed perl impl start */


return 0;

/* embed perl iml stop */


/* gnu regex impl start
  int i, status;
  regex_t start_state;
//  const char *pattern = "^[ \\t]*(state)[ \\t]*:.*$";
//  const char *str = "hello world...";
  const char *pattern = regexParam;
  const char *str = strParam;


  if (regcomp(&start_state, pattern, REG_EXTENDED)) {
    fprintf(stderr, "%s: bad pattern: '%s'\n", str, pattern);
    return 1;
  }

  status = regexec(&start_state, str, 0, NULL, 0);
  regfree(&start_state);
  return status;
gnu regex impl end */


/* pcre2 impl start 
    int rc, i, error;
    PCRE2_SIZE erroffset, *ovector;
    pcre2_code *re;
    pcre2_match_data *match_data;
    PCRE2_SPTR regex = regexParam;
    PCRE2_SPTR str   = strParam;
    size_t len = strlen( (char *)str );
    re = pcre2_compile( regex, PCRE2_ZERO_TERMINATED, 0, &error, &erroffset, NULL );

    if ( !re ) {
      printf( "pcre_compile failed (offset: %d), %d\n", erroffset, error );
      return -1;
    }

    match_data = pcre2_match_data_create_from_pattern( re, NULL );
    rc = pcre2_match( re, str, len, 0, 0, match_data, NULL );
    ovector = pcre2_get_ovector_pointer(match_data);

 //   free(ovector);
    free(match_data);
    free(re);

    return rc;
 pcre2 impl end */
}


int eco_replace(char *regexParam, char *strParam){

/* embed perl impl start */
  char *args[] = { regexParam, strParam };

  dSP;
  ENTER;
  SAVETMPS;
  PUSHMARK(SP);
  XPUSHs(sv_2mortal(newSVpv(args[0], 0)));
  XPUSHs(sv_2mortal(newSVpv(args[1], 0)));
  PUTBACK;

  int iRet = call_pv("eco_replace", G_SCALAR);

  SPAGAIN;
  //printf("return string: %s\n", POPp);

  PUTBACK;
  FREETMPS;
  LEAVE;

return 0;

/* embed perl impl end */

/* pcre2 impl start
    int rc, i, error;
    PCRE2_SIZE erroffset, *ovector;
    pcre2_code *re;
    pcre2_match_data *match_data;
    PCRE2_SPTR regex = (PCRE2_SPTR)regexParam;
    PCRE2_SPTR str   = (PCRE2_SPTR)strParam;
    size_t len = strlen( (char *)str );
    re = pcre2_compile( regex, PCRE2_ZERO_TERMINATED, 0, &error, &erroffset, NULL );


    if ( !re ) {
      printf( "pcre_compile failed (offset: %d), %d\n", erroffset, error );
      return -1;
    }

    match_data = pcre2_match_data_create_from_pattern( re, NULL );


printf("reg after call1: %i\n", retLength);

    rc = pcre2_substitute(re, str,
         strlen(str), 
         0, 
         PCRE2_SUBSTITUTE_GLOBAL|PCRE2_NO_UTF_CHECK,
         match_data,
         NULL, repParam,
         strlen(repParam), 
         ret,
         retLength);

PCRE2_UCHAR buffer[256];
  pcre2_get_error_message(rc, buffer, sizeof(buffer));
  printf("PCRE2 compilation failed at offset %d: %s\n", (int)rc,
    buffer);
printf("reg after call2: %i\n", rc);


//    ovector = pcre2_get_ovector_pointer(match_data);


//    free(ovector);
    free(match_data);
    free(re);

    return rc;
pcre2 impl end */
}


int parseClusterConf(char *ecoConfFile, EcoClusterConf *conf){
  FILE *f;
  char * line = NULL;
  size_t len = 0;
  ssize_t read;

  conf->bufferSize = 1024;
  conf->confFile = ecoConfFile;


  f = fopen(ecoConfFile, "r");
  if (f == NULL){
       //printf("Configuration file not found: %s\n", ecoConfFile);
       //exit(EXIT_FAILURE);
       return ECO_CONF_ERR_FILE_NO_FOUND;
  }

  ECO_LAST_ERROR = "No errors with modifications...";

  while ((read = getline(&line, &len, f)) != -1) {
      //printf("Retrieved line of length %zu :\n", read);
      //printf("%s", line);
  }


  if (line)
           free(line);

  fclose(f);
  return ECO_CONF_OK;
}


int parseDataNodeList(int dataNodesMax, char *ecoConfFile){
  return ECO_CONF_OK;
}

void helloWorld (char *text){
   printf("lib changed ok: %s\n", text);
}


