
/*
    This file is part of intelligend-eco.

    Copyright(c) 2015 Michael Kankkonen

    Inteligend-eco is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intelligend-eco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with intelligend-eco.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INTELLIGEND_COMMON
#define INTELLIGEND_COMMON

extern int ECO_CONF_OK;
extern int ECO_CONF_ERR_FILE_NO_FOUND;
extern char *ECO_LAST_ERROR;

typedef struct
{
  char* dataNodeHost;
  int dataNodePort;
} EcoDataNode;

typedef struct
{
  char* confFile;
  int bufferSize;
  int clusterDataNodeMax;
  int clusterDataNodePort;
  int clusterNodeMasterPort;
  EcoDataNode *dataNodes;
} EcoClusterConf;



extern int parseClusterConf(char *ecoConfFile, EcoClusterConf *conf);
extern int parseDataNodeList(int dataNodesMax, char *ecoConfFile);
extern int eco_replace(char *regex, char *str);
extern int eco_match(char *regex, char *str);
extern int init_perl();
extern int free_perl();


// dummy example method...
extern void helloWorld (char *test);

#endif


