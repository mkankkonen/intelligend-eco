# Intelligend-ECO

Intelligend-ECO is intelligend.com Open Source project for creating a simple, fast and server friendly Big Data platform that can be run even on small linux boxes. The ultimate goals for this projects are

- Save server costs when setting up a Big Data cluster
- Speed up the data processing by keeping the architecture as simple as possible

Below is a overview architecture picture of planned platform. For more information, please look the Wiki.
  

[Intelligend-DB (Database)](https://bitbucket.org/mkankkonen/intelligend-eco/wiki/Intelligend-DB%20(Database))  
[Intelligend-DS (DataService)](https://bitbucket.org/mkankkonen/intelligend-eco/wiki/Intelligend-DS%20(DataService))  
[Intelligend-NM (NodeMaster)](https://bitbucket.org/mkankkonen/intelligend-eco/wiki/Intelligend-NM%20(NodeMaster))  
[Intelligend-FS (FileSystem)](https://bitbucket.org/mkankkonen/intelligend-eco/wiki/Intelligend-FS%20(FileSystem))  
[Intelligend-MR (MapReduce)](https://bitbucket.org/mkankkonen/intelligend-eco/wiki/Intelligend-MR%20(MapReduce))  
  

![eco-bigpicture-small.png](https://bitbucket.org/repo/kz9pdL/images/3990818854-eco-bigpicture-small.png)