/*
    This file is part of intelligend-eco.

    Copyright(c) 2015 Michael Kankkonen

    Inteligend-eco is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Intelligend-eco is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with intelligend-eco.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <zmq.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

int main (void)
{
    void *context = zmq_ctx_new ();
    void *responder = zmq_socket (context, ZMQ_REP);
    int rc = zmq_bind (responder, "tcp://*:5555");
    assert (rc == 0);

    while (1) {
        char buffer [100];
        zmq_recv (responder, buffer, 100, 0);
        printf ("Received %s\n", buffer);
        zmq_recv (responder, buffer, 100, 0);
        printf ("Received %s\n", buffer);
        sleep (1);
        zmq_send (responder, "World", 5, 0);
        printf ("Resoponse sent.\n");
        zmq_close(responder);
        zmq_ctx_destroy (context);
        return 0;
    }

    zmq_ctx_destroy (context);
    return 0;
}

